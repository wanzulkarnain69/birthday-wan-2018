<?php
require '../vendor/autoload.php';
session_start();
if (isset($_SESSION['account'])) {
    exit('This is such a mess. How much you want??');
}

use Billplz\API;
use Billplz\Connect;

if (getenv('stillopen') === 'no') {
    exit('Sorry! No more budget');
}

if (!isset($_POST['name']) || !isset($_POST['bank_code']) || !isset($_POST['account_number']) || !isset($_POST['identifier']) || !isset($_POST['email'])) {
    exit('This is such a mess');
}

$name = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST['name']);
$bank_code = preg_replace("/[^A-Z0-9]+/", "", $_POST['bank_code']);
$account_number = preg_replace("/[^0-9]+/", "", $_POST['account_number']);
$identifier = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST['identifier']);
$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

$connect = (new Connect(getenv('billplz_api_key')))->detectMode();
$parameter = array(
    'mass_payment_instruction_collection_id' => getenv('billplz_mp_collection_id'),
    'bank_code'=> $bank_code,
    'bank_account_number' => $account_number,
    'identity_number'=>$identifier,
    'name' => $name,
    'description' => 'Birthday Give',
    'total' => getenv('amount')
);
$optional = array(
    'email' => $email,
    'notification'=>'true',
    'recipient_notification' => 'true'
);
$billplz = new API($connect);
list($rheader, $rbody) = $billplz->toArray($billplz->createMPI($parameter, $optional));

if ($rheader === 200) {
    echo "You got {$parameter['total']} cents as my birthday give";
    $_SESSION['account'] = 'set';
} else {
    echo 'Not your luck perhaps. Try with another account';
}

if (getenv('development') === 'true') {
    echo '<br>';
    var_dump($rbody);
}
